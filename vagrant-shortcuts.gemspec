# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vagrant-shortcuts/version'

Gem::Specification.new do |spec|
  spec.name          = "vagrant-shortcuts"
  spec.version       = VagrantShortcuts::VERSION
  spec.authors       = ["Tim Heap"]
  spec.email         = ["tim@timheap.me"]
  spec.summary       = "Embed shortcut commands in your Vagrantfile"
  spec.description   = "Embed shortcut commands directly in your Vagrant file"
  spec.homepage      = "https://bitbucket.org/tim_heap/vagrant-shortcuts"
  spec.license       = "BSD"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
