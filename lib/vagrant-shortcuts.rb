require "vagrant-shortcuts/version"

module VagrantPlugins

  module Shortcuts

    # Get some i18n going
    def self.setup_i18n
      I18n.load_path << File.expand_path('locales/en.yml', source_root)
      I18n.reload!
    end

    def self.source_root
      @source_root ||= Pathname.new(File.expand_path("../../", __FILE__))
    end

    # `Shortcut` handles running shortcuts and any associated options
    class Shortcut
      def initialize(callable, start_machine: true)
        @callable = callable
        @start_machine = start_machine
      end

      def call(machine)
        if @start_machine
          machine.action(:up, {:provision_ignore_sentinel => false})
        end
        @callable.call(machine)
      end
    end

    class Plugin < Vagrant.plugin("2")
      name "Shortcuts plugin"
      description <<-DESC
      This plugin allows defining shortcut commands in a Vagrantfile
      DESC

      # Where the real work is done: Running a shortcut
      command "do" do
        class Command < Vagrant.plugin("2", :command)
          def initialize(argv, env)
            super
            @main_args, @sub_command, @sub_args = split_main_and_subcommand(argv)

            # Not a great spot for this, but I dont know where else to put it.
            Shortcuts.setup_i18n
          end

          def execute
            with_target_vms(@sub_args) do |machine|

              if @sub_command == nil
                raise MissingShortcutError.new
              end

              shortcut_name = @sub_command.to_sym
              shortcuts = machine.config.shortcuts

              if not shortcuts.has?(shortcut_name)
                raise UnknownShortcutError.new shortcut: shortcut_name
              end

              shortcut = machine.config.shortcuts.get(shortcut_name)
              shortcut.call(machine)
            end
            0
          end
        end
        Command
      end

      # Add a new config group for shortcuts.
      # End-users should only really care about `add`
      config "shortcuts" do
        class Config < Vagrant.plugin(2, :config)
          attr_accessor :shortcuts

          def initialize
            @shortcuts = {}
          end
          def add(name, *args, &block)
            @shortcuts[name] = Shortcut.new(block, *args)
          end
          def get(name)
            @shortcuts[name]
          end
          def has?(name)
            return @shortcuts.key?(name)
          end
        end
        Config
      end
    end

    # For when stuff breaks
    class ShortcutsError < Vagrant::Errors::VagrantError
      error_namespace("vagrant.plugins.shortcuts")
    end

    class MissingShortcutError < ShortcutsError
      error_key(:missing_shortcut)
    end

    class UnknownShortcutError < ShortcutsError
      error_key(:unknown_shortcut)
    end
  end
end
