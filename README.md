# Vagrant::Shortcuts

Add shortcut commands to your Vagrantfile:

```ruby
config.shortcuts.add(:assets) do |machine|
  machine.action(:ssh_run, ssh_run_command: <<-cmd)
    gulp assets
  cmd
end
```

```bash
$ vagrant do assets
```

## Installation

Install the gem:

    $ gem install vagrant-shortcuts

## Usage

### Defining shortcuts

In your `Vagrantfile`, in a `Vagrant.configure` block,
add any number of shortcuts.
To define a shortcut called `assets` that precompiles all your frontend assets
using the Rails asset pipeline, you might do something like:

```ruby
Vagrant.configure("2") do |config|

  # ...

  config.shortcuts.add(:assets) do |machine|
    machine.action(:ssh_run, ssh_run_command: <<-cmd)
      bin/rake assets:precompile
    cmd
  end
end
```

Shortcuts are defined by a name and a ruby function.
The function must take one argument: a Vagrant machine.
The function may do anything it wants with this machine,
but common tasks include SSHing in to the box and running a command.

By default, the machine is started before the shortcut is run.
This may involve fetching a base box and provisioning the machine.
If you do not want to start the box by default,
pass `start_machine: false` when adding the shortcut:

```ruby
config.shortcuts.add(:assets, start_machine: False) do |machine|
  # ...
end
```

The machine can be started manually by:

```ruby
machine.action(:up, {:provision_ignore_sentinel => false})
```

### Running shortcuts

Run a shortcut from the command line using `vagrant do <shortcut>`.
To run the example `assets` shortcut defined previously, run:

```bash
$ vagrant do assets
```

## Contributing

1. Fork it ( <https://bitbucket.org/tim_heap/vagrant-shortcuts/fork> )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
